<?php
// Verifica se foi feita alguma busca
// Caso contrario, redireciona o visitante pra home
echo '<div class="ui center aligned container">';
if (!isset($_GET['consulta'])) {
  //echo "<h4><i class='users icon'></i>Clientes</h4>";
  exit;
}
// Conecte-se ao MySQL antes desse ponto
// Salva o que foi buscado em uma variável
$busca = mysql_real_escape_string($_GET['consulta']);
// ============================================
// Monta outra consulta MySQL para a busca
$sql = "select * from tec_customers where phone like '%".$busca."%'";
// Executa a consulta
$query = mysql_query($sql);

// =============================================
// Verifica se Consulta retorna vazia. Se sim, cadastra cliente.
if (mysql_num_rows($query) <= 0) {
    echo "Telefone não encontrado... Deseja cadastrar cliente? ";
    echo "<br><br>";
    echo " <a href='../customers/add' class='ui teal mini button'>Sim</a><a href='delivery.php' class='ui mini button'>Não</a>";
    exit;
}
// ============================================
// Começa a exibição dos resultados
echo "<h4><i class='users icon'></i>Clientes</h4>";
echo "<table class='ui celled table'>";
echo "<thead>
          <th>id</th>
          <th>Nome</th>
          <th>Telefone</th>
          <th>Email</th>
          <th>Endereço</th>
          <th class='ui center aligned'>Ações</th>
        </thead>";
while ($resultado = mysql_fetch_assoc($query)) {
  $nome = $resultado['name'];
  $telefone = $resultado['phone'];
  $email = $resultado['email'];
  $endereco = $resultado['endereco'];
  $id = $resultado['id'];
  
  #$link = '/noticia.php?id=' . $resultado['id'];

      echo "<tr>";
    #echo "<a href='{$link}'>";
    echo "<td>";
    echo "{$id}";
    echo "</td>";
    echo "<td>";
    echo "{$nome}";
    echo "</td>";
    echo "<td>";
    echo "{$telefone}";
    echo "</td>";
    echo "<td>";
    echo "{$email}";
    echo "</td>";
    echo "<td>";
    echo "{$endereco}";
    echo "</td>";
    echo "<td class='ui center aligned'>";
    echo '<a href="testes.php?endereco=' . $endereco . '" target="_blank" class="ui mini basic button"><i class="map icon"></i>Mapa</a>';
    echo '<a href="escolhe_cliente.php?cliente='.$nome.'&id='.$id.'" class="ui teal mini button">Selecionar</a>';
    echo "</td>";
    #echo "</a>";
  echo "</tr>";
}
echo "</table>";
echo '</div>';
?>